package pe.gob.bnp.tools;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicates;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
//import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket commonServicesApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select().apis(RequestHandlerSelectors.basePackage("pe.gob.bnp.tools"))
                .paths(Predicates.not(PathSelectors.regex("/error"))) //.paths(regex("/common"))
                .build()
                .apiInfo(apiInfo());
    	/*return new Docket(DocumentationType.SWAGGER_2).
    			host("www.mydomain.com")
    		    .pathProvider(new RelativePathProvider(ServletContext servletContext) {
    		        @Override
    		        public String getApplicationBasePath() {
    		            return "/servicioscomunes";
    		        }
    		    });*/
    }
    
    private ApiInfo apiInfo() {
        String description = "API REST de Servicios comunes usando Clean Architecture - Implementado por OTIE-BNP";
        return new ApiInfoBuilder()
                .title("API REST para Servicios comunes")
                .description(description)
                .termsOfServiceUrl("BNP")
                .license("OTIE")
                .licenseUrl("www.bnp.gob.pe")
                .version("v1.0")
                .build();
    }    
}
