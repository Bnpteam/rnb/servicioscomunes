package pe.gob.bnp.tools.smb.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiParam;
import pe.gob.bnp.tools.ad.aplicacion.ServicioAplicacionAD;
import pe.gob.bnp.tools.ad.aplicacion.dto.AccesoLdapResponse;
import pe.gob.bnp.tools.ad.aplicacion.dto.CredencialAccesoDTO;
import pe.gob.bnp.tools.comun.api.controlador.ResponseHandler;
import pe.gob.bnp.tools.comun.aplicacion.EntityNotFoundResultException;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("api/serviciosSMB/")
public class SMBServicesController {
	@Autowired
	ServicioAplicacionAD servicioAplicacionAD;
	
	@Autowired
	ResponseHandler responseHandler;
	
	@RequestMapping(method = RequestMethod.POST, path = "/buscarDocumento", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> buscarDocumentoEnRepositorio(
			@RequestBody CredencialAccesoDTO credencialAccesoDTO){ 
		try {
			AccesoLdapResponse response = servicioAplicacionAD.validarUsuarioenAD(credencialAccesoDTO);
			if (response == null) {
				return this.responseHandler.getNotFoundObjectResponse("Error al momento de realizar consulta a Active Directory.");
			}
			return this.responseHandler.getOkObjectResponse(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("Error al momento de realizar consulta a Active Directory.", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}
	}
	
	/*
	@RequestMapping(method = RequestMethod.GET, path = "/test", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	*/
	/*
	public ResponseEntity<Object> validarAccesoActiveDirectory(){ 
		try {
			return this.responseHandler.getOkCommandResponse("hello world");
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("Error al momento de realizar consulta a Active Directory.", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}
	}	*/
}
