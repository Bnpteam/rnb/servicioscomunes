package pe.gob.bnp.tools.smb.aplicacion.dto;

public class FileResponse {
	public String codigoRespuesta;
	public String descripcionRespuesta;
	public byte[] contenido;
	
	public FileResponse() {
		this.codigoRespuesta = "9999";
		this.descripcionRespuesta = "N/A";
		this.contenido = null;
	}
	
	public FileResponse(String codigoRespuesta, String descripcionRespuesta, byte[] contenido) {
		super();
		this.codigoRespuesta = codigoRespuesta;
		this.descripcionRespuesta = descripcionRespuesta;
		this.contenido = contenido;
	}

	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}
	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}
	public String getDescripcionRespuesta() {
		return descripcionRespuesta;
	}
	public void setDescripcionRespuesta(String descripcionRespuesta) {
		this.descripcionRespuesta = descripcionRespuesta;
	}
	public byte[] getContenido() {
		return contenido;
	}
	public void setContenido(byte[] contenido) {
		this.contenido = contenido;
	}
	
	
}
