package pe.gob.bnp.tools.reniec.aplicacion.dto;

import pe.gob.bnp.tools.comun.aplicacion.dto.RequestBaseDto;
import pe.gob.bnp.tools.reniec.dominio.entidad.Persona;


public class PersonaDTO extends RequestBaseDto {
	String dni;
	String primerApellido;
	String segundoApellido;
	String direccion;
	String estadoCivil;
	byte[] foto;
	String nombre;
	String ubigeo;
	String restriccion;
	String coResultado;
	String deResultado;
	
	
	
	public PersonaDTO() {
		super();
	}

	public PersonaDTO(Persona objeto, String coResultado, String deResultado) {
		super();
		this.dni = objeto.getDni();
		this.primerApellido = objeto.getPrimerApellido();
		this.segundoApellido = objeto.getSegundoApellido();
		this.direccion = objeto.getDireccion();
		this.estadoCivil = objeto.getEstadoCivil();
		this.foto = objeto.getFoto();
		this.nombre = objeto.getNombre();
		this.ubigeo = objeto.getUbigeo();
		this.restriccion = objeto.getRestriccion();
		this.coResultado = coResultado;
		this.deResultado = deResultado;
	}
	
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getPrimerApellido() {
		return primerApellido;
	}
	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}
	public String getSegundoApellido() {
		return segundoApellido;
	}
	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getEstadoCivil() {
		return estadoCivil;
	}
	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	public byte[] getFoto() {
		return foto;
	}
	public void setFoto(byte[] foto) {
		this.foto = foto;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getUbigeo() {
		return ubigeo;
	}
	public void setUbigeo(String ubigeo) {
		this.ubigeo = ubigeo;
	}
	public String getRestriccion() {
		return restriccion;
	}
	public void setRestriccion(String restriccion) {
		this.restriccion = restriccion;
	}
	public String getCoResultado() {
		return coResultado;
	}
	public void setCoResultado(String coResultado) {
		this.coResultado = coResultado;
	}
	public String getDeResultado() {
		return deResultado;
	}
	public void setDeResultado(String deResultado) {
		this.deResultado = deResultado;
	}	
	
	
}
