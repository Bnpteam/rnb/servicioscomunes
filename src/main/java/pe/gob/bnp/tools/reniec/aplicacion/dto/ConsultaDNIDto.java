package pe.gob.bnp.tools.reniec.aplicacion.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import pe.gob.bnp.tools.comun.aplicacion.dto.RequestBaseDto;
import pe.gob.bnp.tools.comun.aplicacion.enumeracion.RequestBodyType;
import pe.gob.bnp.tools.reniec.aplicacion.dto.deserializer.ConsultaDNIDtoDeserializer;

@JsonDeserialize(using = ConsultaDNIDtoDeserializer.class)
public class ConsultaDNIDto extends RequestBaseDto	 {
	private String dniConsultado;

	public ConsultaDNIDto(String dniConsultado, RequestBodyType requestBodyType) {
		super();
		this.dniConsultado = dniConsultado;
		this.requestBodyType = requestBodyType;
	}

	public String getDniConsultado() {
		return dniConsultado;
	}

	public void setDniConsultado(String dniConsultado) {
		this.dniConsultado = dniConsultado;
	}

	
}
