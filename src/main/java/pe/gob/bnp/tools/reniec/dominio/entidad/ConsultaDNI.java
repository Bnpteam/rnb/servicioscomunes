package pe.gob.bnp.tools.reniec.dominio.entidad;

public class ConsultaDNI {
	private String dniConsulta;
	private String dniAutenticacion;
	
	public String getDniConsulta() {
		return dniConsulta;
	}
	public void setDniConsulta(String dniConsulta) {
		this.dniConsulta = dniConsulta;
	}
	public String getDniAutenticacion() {
		return dniAutenticacion;
	}
	public void setDniAutenticacion(String dniAutenticacion) {
		this.dniAutenticacion = dniAutenticacion;
	}
	
	
}
