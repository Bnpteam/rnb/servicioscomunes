package pe.gob.bnp.tools.reniec.aplicacion;

import java.rmi.RemoteException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;

import pe.gob.bnp.tools.comun.aplicacion.Notification;
import pe.gob.bnp.tools.comun.aplicacion.dto.mapper.PersonaMapper;
import pe.gob.bnp.tools.comun.util.VO;
import pe.gob.bnp.tools.reniec.aplicacion.dto.PersonaDTO;
import pe.gob.bnp.tools.reniec.dominio.entidad.Persona;
import pe.gob.bnp.tools.reniec.dominio.servicio.ServicioDominioReniec;
import pe.gob.reniec.ws.PeticionConsulta;
import pe.gob.reniec.ws.ReniecConsultaDniPortTypeProxy;
import pe.gob.reniec.ws.ResultadoConsulta;


@Service
@CrossOrigin(origins = "*")
public class ServicioAplicacionReniec {
	@Autowired
	ServicioDominioReniec servicioDominioReniec;
	
	@Autowired
	PersonaMapper personaMapper;
	
	@Value("${url.api.soap.reniec.consultadni}")
	String urlConsultaReniec;
	
	@Value("${reniec.credencial.usuario}")
	String usuarioCredencialReniec;
	
	@Value("${reniec.credencial.clave}")
	String claveCredencialReniec;
	
	@Value("${reniec.credencial.ruc}")
	String rucCredencialReniec;
	
	//@Transactional
	public PersonaDTO obtenerDatosPorNumeroDNI(String dni) throws Exception {
		Notification notification = this.validation(dni);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}
		PersonaDTO personaDTO = this.consultarDNIenReniec(dni);
		return personaDTO;		
	}
	
	public PersonaDTO obtenerDatosPorNumeroDNI(String dniConsulta, String dniAutenticacion) throws Exception {
		Notification notification = this.validation(dniConsulta);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}
		PersonaDTO personaDTO = this.consultarDNIenReniec(dniConsulta,dniAutenticacion);
		return personaDTO;		
	}	
	
	public PersonaDTO consultarDNIenReniec(String dni) throws RemoteException {
		ReniecConsultaDniPortTypeProxy reniecConsultaDniPortTypeProxy = new ReniecConsultaDniPortTypeProxy();
		reniecConsultaDniPortTypeProxy.setEndpoint(VO.getString(urlConsultaReniec));
		
		
		PeticionConsulta peticionConsulta = new PeticionConsulta();
		peticionConsulta.setNuDniConsulta(dni);
		peticionConsulta.setNuDniUsuario(VO.getString(usuarioCredencialReniec));
		peticionConsulta.setNuRucUsuario(VO.getString(rucCredencialReniec));
		peticionConsulta.setPassword(VO.getString(claveCredencialReniec));
		
		ResultadoConsulta resultadoConsulta = new ResultadoConsulta();
		resultadoConsulta = reniecConsultaDniPortTypeProxy.consultar(peticionConsulta);
		
		
		Persona persona = new Persona();
		if (resultadoConsulta.getCoResultado().equals("0000")) {
			persona.setNombre(resultadoConsulta.getDatosPersona().getPrenombres().toUpperCase().trim());
			persona.setPrimerApellido(resultadoConsulta.getDatosPersona().getApPrimer().toUpperCase().trim());
			persona.setSegundoApellido(resultadoConsulta.getDatosPersona().getApSegundo().toUpperCase().trim());
			persona.setDireccion(resultadoConsulta.getDatosPersona().getDireccion());
			persona.setEstadoCivil(resultadoConsulta.getDatosPersona().getEstadoCivil());
			persona.setFoto(resultadoConsulta.getDatosPersona().getFoto());
			persona.setRestriccion(resultadoConsulta.getDatosPersona().getRestriccion());
			persona.setUbigeo(resultadoConsulta.getDatosPersona().getUbigeo());
			persona.setDni(dni);
		}
		PersonaDTO dto = personaMapper.mapper(persona, resultadoConsulta.getCoResultado(), resultadoConsulta.getDeResultado());
		
		return dto;
		
	}
	
	public PersonaDTO consultarDNIenReniec(String dniConsulta, String dniAutenticacion) throws RemoteException {
		ReniecConsultaDniPortTypeProxy reniecConsultaDniPortTypeProxy = new ReniecConsultaDniPortTypeProxy();
		reniecConsultaDniPortTypeProxy.setEndpoint(VO.getString(urlConsultaReniec));
		
		
		PeticionConsulta peticionConsulta = new PeticionConsulta();
		peticionConsulta.setNuDniConsulta(dniConsulta);
		peticionConsulta.setNuDniUsuario(dniAutenticacion);
		peticionConsulta.setNuRucUsuario(VO.getString(rucCredencialReniec));
		peticionConsulta.setPassword(dniAutenticacion);
		
		ResultadoConsulta resultadoConsulta = new ResultadoConsulta();
		resultadoConsulta = reniecConsultaDniPortTypeProxy.consultar(peticionConsulta);
		
		
		Persona persona = new Persona();
		if (resultadoConsulta.getCoResultado().equals("0000")) {
			persona.setNombre(resultadoConsulta.getDatosPersona().getPrenombres().toUpperCase().trim());
			persona.setPrimerApellido(resultadoConsulta.getDatosPersona().getApPrimer().toUpperCase().trim());
			persona.setSegundoApellido(resultadoConsulta.getDatosPersona().getApSegundo().toUpperCase().trim());
			persona.setDireccion(resultadoConsulta.getDatosPersona().getDireccion());
			persona.setEstadoCivil(resultadoConsulta.getDatosPersona().getEstadoCivil());
			persona.setFoto(resultadoConsulta.getDatosPersona().getFoto());
			persona.setRestriccion(resultadoConsulta.getDatosPersona().getRestriccion());
			persona.setUbigeo(resultadoConsulta.getDatosPersona().getUbigeo());
			persona.setDni(dniConsulta);
		}
		PersonaDTO dto = personaMapper.mapper(persona, resultadoConsulta.getCoResultado(), resultadoConsulta.getDeResultado());
		
		return dto;
		
	}	
	
	/*
	private Notification validation(ConsultaDNIDto consultaDNIDto) {
		Notification notification = new Notification();
		if (consultaDNIDto == null) {
			notification.addError("Request inválido.");
			return notification;
		}
		if (consultaDNIDto.getRequestBodyType() != RequestBodyType.VALID)   {
			notification.addError("Datos Json inválidos.");
		}
		if (consultaDNIDto.getDniConsultado() == null || consultaDNIDto.getDniConsultado().equals("")) {
			notification.addError("Dni no ha sido ingresado.");
		}
		if (consultaDNIDto.getDniConsultado().length() != 8) {
			notification.addError("Dni debe tener 8 digitos.");
		}
		return notification;
	}
	*/
	private Notification validation(String dni) {
		Notification notification = new Notification();
		if (dni == null || dni.equals("")) {
			notification.addError("Dni no ha sido ingresado.");
		}
		if (dni.length() != 8) {
			notification.addError("Dni debe tener 8 digitos.");
		}
		return notification;
	}	
	
}
