package pe.gob.bnp.tools.reniec.api.controlador;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.bnp.tools.comun.api.controlador.ResponseHandler;
import pe.gob.bnp.tools.comun.aplicacion.EntityNotFoundResultException;
import pe.gob.bnp.tools.comun.aplicacion.enumeracion.RequestBodyType;
import pe.gob.bnp.tools.reniec.aplicacion.ServicioAplicacionReniec;
import pe.gob.bnp.tools.reniec.aplicacion.dto.PersonaDTO;
import pe.gob.bnp.tools.reniec.dominio.entidad.ConsultaDNI;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("api/serviciosReniec/")
@Api(value = "/api/serviciosReniec/",description="Servicio REST de consulta a RENIEC - Implementado por OTIE/BNP")
public class ReniecServicesController {
	
	@Autowired
	ServicioAplicacionReniec servicioAplicacionReniec;
	
	@Autowired
	ResponseHandler responseHandler;
	
	@Autowired
	ModelMapper modelMaper;
	
	@RequestMapping(method = RequestMethod.GET, path = "consultarDNI/{dni}",  produces = {MediaType.APPLICATION_JSON_VALUE} )
	@ApiOperation(value = "Consulta de DNI", response= PersonaDTO.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 400, message = "Solicitud contiene errores."),
        @ApiResponse(code = 404, message = "No se pudo obtener datos de la persona."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la petición"),
	})		
	public ResponseEntity<Object> consultarDNI(
			@ApiParam(value = "Número de documento de identidad consultado.", required = true)
			@PathVariable("dni") String dni){ 
		try {
			PersonaDTO personaDTO = servicioAplicacionReniec.obtenerDatosPorNumeroDNI(dni);
			if (personaDTO== null) {
				return this.responseHandler.getNotFoundObjectResponse("No se ha encontrado información para el DNI");
			}
			personaDTO.setRequestBodyType(RequestBodyType.VALID);
			return this.responseHandler.getOkObjectResponse(personaDTO);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("Persona no encontrada", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "consultarDNI",  produces = {MediaType.APPLICATION_JSON_VALUE} )
	@ApiOperation(value = "Consulta de DNI", response= PersonaDTO.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 400, message = "Solicitud contiene errores."),
        @ApiResponse(code = 404, message = "No se pudo obtener datos de la persona."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la petición"),
	})		
	public ResponseEntity<Object> consultarDNI(
			@ApiParam(value = "bean que contiene el dni de consulta y el dni para la autenticación", required = true)
			@RequestBody  ConsultaDNI datos){ 
		try {
			PersonaDTO personaDTO = servicioAplicacionReniec.obtenerDatosPorNumeroDNI(datos.getDniConsulta(), datos.getDniAutenticacion());
			if (personaDTO== null) {
				return this.responseHandler.getNotFoundObjectResponse("No se ha encontrado información para el DNI");
			}
			personaDTO.setRequestBodyType(RequestBodyType.VALID);
			return this.responseHandler.getOkObjectResponse(personaDTO);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("Persona no encontrada", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}
	}	
	
	/*
	@RequestMapping(method = RequestMethod.POST, path = "/listarDNIs", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> listarDNIs(){ 
		try {
			List<PersonaDTO> personas = new ArrayList<PersonaDTO>();
			Persona persona01 = new Persona("10101001", "NUÑEZ", "PEREZ", "JR.SAN MARTIN 545 DPTO.501", "CASADO",null,  "GONZALO", "LIMA/LIMA/MIRAFLORES", "NINGUNA");
			Persona persona02 = new Persona("10101002", "CCASA", "CONDORI", "JR.SAN MARTIN 546 DPTO.501", "CASADO",null,  "ALFREDO", "LIMA/LIMA/SAN JUAN", "NINGUNA");
			Persona persona03 = new Persona("10101003", "CAMPOS", "RIVAS", "JR.SAN MARTIN 547 DPTO.501", "SOLTERO",null,  "BRANDON", "LIMA/LIMA/SANTA ANITA", "NINGUNA");
			Persona persona04 = new Persona("10101004", "ROSSI", "GARAY", "JR.SAN MARTIN 548 DPTO.501", "CASADO",null,  "MATEO", "LIMA/LIMA/EL AGUSTINO", "NINGUNA");
			Persona persona05 = new Persona("10101005", "ESPOSITO", "QUISPE", "JR.SAN MARTIN 549 DPTO.501", "SOLTERO",null,  "DANIEL", "LIMA/LIMA/SAN MIGUEL", "NINGUNA");
			Persona persona06 = new Persona("10101006", "RUSSO", "PAREDES", "JR.SAN MARTIN 550 DPTO.501", "CASADO",null,  "PABLO", "LIMA/LIMA/LOS OLIVOS", "NINGUNA");
			Persona persona07 = new Persona("10101007", "ROMANO", "HUAMAN", "JR.SAN MARTIN 551 DPTO.501", "CASADO",null,  "ALVARO", "LIMA/LIMA/SAN JUAN DE MIRAFLORES", "NINGUNA");
			Persona persona08 = new Persona("10101008", "BIANCHI", "CESPEDES", "JR.SAN MARTIN 552 DPTO.501", "CASADO",null,  "FELIPE", "LIMA/LIMA/CHORRILLOS", "NINGUNA");
			Persona persona09 = new Persona("10101009", "FERRARI", "MAESTRI", "JR.SAN MARTIN 553 DPTO.501", "CASADO",null,  "ADRIÁN", "LIMA/LIMA/MIRAFLORES", "NINGUNA");
			Persona persona10 = new Persona("10101010", "MARINO", "PALACIOS", "JR.SAN MARTIN 554 DPTO.501", "CASADO",null,  "DAVID", "LIMA/LIMA/MIRAFLORES", "NINGUNA");
			Persona persona11 = new Persona("10101011", "RIZZO", "SOLANO", "JR.SAN MARTIN 555 DPTO.501", "SOLTERO",null,  "DIEGO", "LIMA/LIMA/MIRAFLORES", "NINGUNA");
			Persona persona12 = new Persona("10101012", "GALLO", "CUBILLAS", "JR.SAN MARTIN 556 DPTO.501", "SOLTERO",null,  "MARIO", "LIMA/LIMA/MIRAFLORES", "NINGUNA");
			Persona persona13 = new Persona("10101013", "RICCI", "SOTIL", "JR.SAN MARTIN 557 DPTO.501", "SOLTERO",null,  "SERGIO", "LIMA/LIMA/MIRAFLORES", "NINGUNA");
			Persona persona14 = new Persona("10101014", "CARUSSO", "AQUINO", "JR.SAN MARTIN 558 DPTO.501", "SOLTERO",null,  "MARCOS", "LIMA/LIMA/MIRAFLORES", "NINGUNA");
			Persona persona15 = new Persona("10101015", "COSTA", "OREJUELA", "JR.SAN MARTIN 559 DPTO.501", "CASADO",null,  "MARTÍN", "LIMA/LIMA/MIRAFLORES", "NINGUNA");
			Persona persona16 = new Persona("10101016", "MANCINI", "RUIDIAZ", "JR.SAN MARTIN 560 DPTO.501", "CASADO",null,  "JORGE", "LIMA/LIMA/MIRAFLORES", "NINGUNA");
			Persona persona17 = new Persona("10101017", "FERRARA", "SAENZ", "JR.SAN MARTIN 561 DPTO.501", "CASADO",null,  "IVÁN", "LIMA/LIMA/MIRAFLORES", "NINGUNA");
			Persona persona18 = new Persona("10101018", "LEONE", "MALDONADO", "JR.SAN MARTIN 562 DPTO.501", "CASADO",null,  "CARLOS", "LIMA/LIMA/PUENTE PIEDRA", "NINGUNA");
			Persona persona19 = new Persona("10101019", "COLOMBO", "YOTUM", "JR.SAN MARTIN 563 DPTO.501", "SOLTERO",null,  "MIGUEL", "LIMA/LIMA/VILLA EL SALVADOR", "NINGUNA");
			Persona persona20 = new Persona("10101020", "SANTORO", "PINEDA", "JR.SAN MARTIN 564 DPTO.501", "CASADO",null,  "LUCAS", "LIMA/LIMA/MIRAFLORES", "NINGUNA");
			Persona persona21 = new Persona("10101021", "CONTI", "MESSI", "JR.SAN MARTIN 565 DPTO.501", "CASADO",null,  "ABDEL", "LIMA/LIMA/MIRAFLORES", "NINGUNA");
			Persona persona22 = new Persona("10101022", "COPPOLA", "MARADONA", "JR.SAN MARTIN 566 DPTO.501", "CASADO",null,  "FARID", "LIMA/LIMA/MIRAFLORES", "NINGUNA");
			Persona persona23 = new Persona("10101023", "AMATO", "HERRERA", "JR.SAN MARTIN 567 DPTO.501", "SOLTERO",null,  "IBRAHIM", "LIMA/LIMA/MIRAFLORES", "NINGUNA");
			Persona persona24 = new Persona("10101024", "BIANCO", "MELCHOR", "JR.SAN MARTIN 568 DPTO.501", "SOLTERO",null,  "WALID", "LIMA/LIMA/MIRAFLORES", "NINGUNA");
			Persona persona25 = new Persona("10101025", "VITALE", "LEGUA", "JR.SAN MARTIN 569 DPTO.501", "SOLTERO",null,  "NADER", "LIMA/LIMA/MIRAFLORES", "NINGUNA");
			Persona persona26 = new Persona("10101026", "MESSINA", "VALENCIA", "JR.SAN MARTIN 570 DPTO.501", "CASADO",null,  "SILVIO", "LIMA/LIMA/MIRAFLORES", "NINGUNA");
			
			personas.add(modelMaper.map(persona01, PersonaDTO.class));
			personas.add(modelMaper.map(persona02, PersonaDTO.class));
			personas.add(modelMaper.map(persona03, PersonaDTO.class));
			personas.add(modelMaper.map(persona04, PersonaDTO.class));
			personas.add(modelMaper.map(persona05, PersonaDTO.class));
			personas.add(modelMaper.map(persona06, PersonaDTO.class));
			personas.add(modelMaper.map(persona07, PersonaDTO.class));
			personas.add(modelMaper.map(persona08, PersonaDTO.class));
			personas.add(modelMaper.map(persona09, PersonaDTO.class));
			personas.add(modelMaper.map(persona10, PersonaDTO.class));
			personas.add(modelMaper.map(persona11, PersonaDTO.class));
			personas.add(modelMaper.map(persona12, PersonaDTO.class));
			personas.add(modelMaper.map(persona13, PersonaDTO.class));
			personas.add(modelMaper.map(persona14, PersonaDTO.class));
			personas.add(modelMaper.map(persona15, PersonaDTO.class));
			personas.add(modelMaper.map(persona16, PersonaDTO.class));
			personas.add(modelMaper.map(persona17, PersonaDTO.class));
			personas.add(modelMaper.map(persona18, PersonaDTO.class));
			personas.add(modelMaper.map(persona19, PersonaDTO.class));
			personas.add(modelMaper.map(persona20, PersonaDTO.class));
			personas.add(modelMaper.map(persona21, PersonaDTO.class));
			personas.add(modelMaper.map(persona22, PersonaDTO.class));
			personas.add(modelMaper.map(persona23, PersonaDTO.class));
			personas.add(modelMaper.map(persona24, PersonaDTO.class));
			personas.add(modelMaper.map(persona25, PersonaDTO.class));
			personas.add(modelMaper.map(persona26, PersonaDTO.class));
			
			return this.responseHandler.getOkObjectResponse(personas);
//			Persona persona = servicioAplicacionReniec.obtenerDatosPorNumeroDNI(new ConsultaDNIDto(dniConsultado, requestBodyType );
//			if (persona== null) {
//				return this.responseHandler.getNotFoundObjectResponse("Persona no encontrada");
//			}
//			PersonaDTO personaDTO = modelMaper.map(persona, PersonaDTO.class);
//			personaDTO.setRequestBodyType(RequestBodyType.VALID);
//			return this.responseHandler.getOkObjectResponse(personaDTO);			
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("lista no encontrada", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}
	}*/	
	
}
