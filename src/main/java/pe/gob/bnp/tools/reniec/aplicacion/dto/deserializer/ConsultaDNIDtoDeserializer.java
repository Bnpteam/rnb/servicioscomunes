package pe.gob.bnp.tools.reniec.aplicacion.dto.deserializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import pe.gob.bnp.tools.comun.aplicacion.enumeracion.RequestBodyType;
import pe.gob.bnp.tools.reniec.aplicacion.dto.ConsultaDNIDto;

public class ConsultaDNIDtoDeserializer extends JsonDeserializer<ConsultaDNIDto> {

	@Override
	public ConsultaDNIDto deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		ConsultaDNIDto dto = null;
		try {
			ObjectCodec objectCodec = jsonParser.getCodec();
			JsonNode jsonNode = objectCodec.readTree(jsonParser);	
			String dniConsultado = jsonNode.get("dniConsultado").asText();
			dto = new ConsultaDNIDto(dniConsultado, RequestBodyType.VALID);
		} catch (Exception e) {
			dto = new ConsultaDNIDto("", RequestBodyType.INVALID);
		}
		return dto;
	}

}
