package pe.gob.bnp.tools.reniec.dominio.entidad;

import org.springframework.stereotype.Component;

@Component
public class Persona {
	private String dni;
	private String primerApellido;
	private String segundoApellido;
	private String direccion;
	private String estadoCivil;
	private byte[] foto;
	private String nombre;
	private String ubigeo;
	private String restriccion;
	
	public Persona() {
		super();
	}
	
	public Persona(String dni, String primerApellido, String segundoApellido, String direccion, String estadoCivil, byte[] foto,
			String nombre, String ubigeo, String restriccion) {
		super();
		this.dni = dni;
		this.primerApellido = primerApellido;
		this.segundoApellido = segundoApellido;
		this.direccion = direccion;
		this.estadoCivil = estadoCivil;
		this.foto = foto;
		this.nombre = nombre;
		this.ubigeo = ubigeo;
		this.restriccion = restriccion;
	}
	public String getPrimerApellido() {
		return primerApellido;
	}
	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}
	public String getSegundoApellido() {
		return segundoApellido;
	}
	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getEstadoCivil() {
		return estadoCivil;
	}
	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	public byte[] getFoto() {
		return foto;
	}
	public void setFoto(byte[] foto) {
		this.foto = foto;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getUbigeo() {
		return ubigeo;
	}
	public void setUbigeo(String ubigeo) {
		this.ubigeo = ubigeo;
	}
	public String getRestriccion() {
		return restriccion;
	}
	public void setRestriccion(String restriccion) {
		this.restriccion = restriccion;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	
}
