package pe.gob.bnp.tools.reniec.dominio.servicio;

import org.springframework.stereotype.Service;

import pe.gob.bnp.tools.reniec.aplicacion.dto.ConsultaDNIDto;
import pe.gob.bnp.tools.reniec.dominio.entidad.Persona;

@Service
public class ServicioDominioReniec {
	
	public Persona consultarPorDNIusandoWSReniec(ConsultaDNIDto consulta){//aqui viene la integración con el ws de reniec.
		return new Persona("10265648","VALVERDE", "RIVERA", "JR.SAN MARTIN 545 DPTO.501", "CASADO", null, "JOSE EDUARDO", "LIMA/LIMA/MIRAFLORES", "FALLECIMIENTO");
	}
}
