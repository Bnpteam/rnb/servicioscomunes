package pe.gob.bnp.tools.email.aplicacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import pe.gob.bnp.tools.email.domain.entity.CredencialRegistrador;
import pe.gob.bnp.tools.email.domain.entity.MensajeTexto;

@Service
public class EmailApplicationService {
	//@Autowired
	JavaMailSender emailSender;
	
	private MailContentBuilder mailContentBuilder;	
	
    @Autowired
    public EmailApplicationService(JavaMailSender emailSender, MailContentBuilder mailContentBuilder) {
        this.emailSender = emailSender;
        this.mailContentBuilder = mailContentBuilder;
    }
    
	public String sendTextMessageEmail(MensajeTexto mensaje) {
		String codigoResultado="";
		try {
			SimpleMailMessage message = new SimpleMailMessage();
			message.setFrom("notificaciones@bnp.gob.pe");
			message.setTo(mensaje.getTo());
			message.setSubject(mensaje.getSubject());
			message.setText(mensaje.getText());
			emailSender.send(message);
			codigoResultado = "0000";
		}catch(MailException e){
			codigoResultado = e.getMessage();
		}
		return codigoResultado;
	}
	
	public String sendHTMLEmailCreacionCuentaRegistrador(CredencialRegistrador credencial ) {
		String codigoResultado="";
	    try {
		    MimeMessagePreparator messagePreparator = mimeMessage -> {
	            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
	            messageHelper.setFrom("notificaciones@bnp.gob.pe");
	            messageHelper.setTo(credencial.getEmailDestino());
	            messageHelper.setSubject(credencial.getTitulo());
	            String content = mailContentBuilder.buildTemplateCreacionCuenta(credencial.getDni(), credencial.getPassword());
	            messageHelper.setText(content, true);
		    };	    	
	    	emailSender.send(messagePreparator);
	    	codigoResultado = "0000";
	    }catch(MailException e) {
	    	codigoResultado = e.getMessage();
	    }
	    return codigoResultado;
	}	
	
}
