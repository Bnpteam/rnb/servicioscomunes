package pe.gob.bnp.tools.email.domain.entity;

public class MensajeTexto {
	private String to;
	private String text;
	private String subject;

	public MensajeTexto() {
		super();
		
	}
	
	public MensajeTexto(String to, String message, String subject) {
		super();
		this.to = to;
		this.text = message;
		this.subject = subject;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}

	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	
}
