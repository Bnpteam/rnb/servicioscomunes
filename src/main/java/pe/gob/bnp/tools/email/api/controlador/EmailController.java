package pe.gob.bnp.tools.email.api.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.bnp.tools.comun.api.controlador.ResponseHandler;
import pe.gob.bnp.tools.email.aplicacion.EmailApplicationService;
import pe.gob.bnp.tools.email.domain.entity.CredencialRegistrador;
import pe.gob.bnp.tools.email.domain.entity.MensajeTexto;

@RestController
@RequestMapping("api/email")
@CrossOrigin(origins = "*")
public class EmailController {
	@Autowired
	EmailApplicationService  emailApplicationService;
	
	@Autowired
	ResponseHandler responseHandler;
	
	@RequestMapping(method = RequestMethod.POST, path = "/simple", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Envío de email que contiene solo un texto plano", response= String.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Operación exitosa"),
        @ApiResponse(code = 500, message = "Error al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> enviarEmailTexto(@RequestBody MensajeTexto mensaje){
		try {
			String resultado = this.emailApplicationService.sendTextMessageEmail(mensaje);
			return new ResponseEntity<Object>(resultado, HttpStatus.OK);
		}catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/creacioncuenta", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Envío de email para creacion de cuenta", response= String.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Operación exitosa"),
        @ApiResponse(code = 400, message = "Solicitud contiene errores"),
        @ApiResponse(code = 404, message = "Error al momento de realizar consulta a Active Directory"),
        @ApiResponse(code = 500, message = "Error al momento de ejecutar la petición"),
	})	
	public ResponseEntity<Object> enviarEmailCreacionCuentaRegistrador(@RequestBody CredencialRegistrador credencial){
		this.emailApplicationService.sendHTMLEmailCreacionCuentaRegistrador(credencial);;
		return new ResponseEntity<Object>("OK", HttpStatus.OK);
	}	
	
}
