package pe.gob.bnp.tools;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.apache.catalina.Context;
import org.apache.tomcat.util.descriptor.web.SecurityCollection;
import org.apache.tomcat.util.descriptor.web.SecurityConstraint;


@SpringBootApplication
public class CommonServicesApplication {
	public static void main(String[] args) {
		SpringApplication.run(CommonServicesApplication.class, args);
	}
	
	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}	
	
	/*
	@Bean
    public ServletWebServerFactory servletContainer() {
        // Enable SSL Trafic
        TomcatServletWebServerFactory tomcat = new TomcatServletWebServerFactory() {
            @Override
            protected void postProcessContext(Context context) {
                SecurityConstraint securityConstraint = new SecurityConstraint();
                securityConstraint.setUserConstraint("CONFIDENTIAL");
                SecurityCollection collection = new SecurityCollection();
                collection.addPattern("/*");
                securityConstraint.addCollection(collection);
                context.addConstraint(securityConstraint);
            }
        };

        // Add HTTP to HTTPS redirect
        //tomcat.addAdditionalTomcatConnectors(httpToHttpsRedirectConnector());

        return tomcat;
    }*/



	
    /*
    We need to redirect from HTTP to HTTPS. Without SSL, this application used
    port 8086. With SSL it will use port 9880. So, any request for 8086 needs to be
    redirected to HTTPS on 9880.
     */
	/*private Connector httpToHttpsRedirectConnector() {
        Connector connector = new Connector(TomcatServletWebServerFactory.DEFAULT_PROTOCOL);
        connector.setScheme("http");
        connector.setPort(8086);
        connector.setSecure(false);
        connector.setRedirectPort(9880);
        return connector;
    }*/

	
	
}
//@SpringBootApplication
//public class CommonServicesApplication extends SpringBootServletInitializer {
//	private static Class<CommonServicesApplication> applicationClass = CommonServicesApplication.class;
//	
//	public static void main(String[] args) {
//		SpringApplication.run(CommonServicesApplication.class, args);
//	}
//	
//	 @Override
//	 protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
//	        return application.sources(applicationClass);
//	  }
//
//
//	@Bean
//	public ModelMapper modelMapper() {
//		return new ModelMapper();
//	}	
//}


