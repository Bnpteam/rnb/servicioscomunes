package pe.gob.bnp.tools.comun.aplicacion.dto;

public class ResponseDto {
	private Object response;

	public Object getResponse() {
		return response;
	}

	public void setResponse(Object response) {
		this.response = response;
	}
}
