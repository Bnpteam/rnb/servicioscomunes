package pe.gob.bnp.tools.comun.aplicacion.dto;

import pe.gob.bnp.tools.comun.aplicacion.enumeracion.RequestBodyType;

public class RequestBaseDto {
	protected RequestBodyType requestBodyType;

	public RequestBodyType getRequestBodyType() {
		return requestBodyType;
	}

	public void setRequestBodyType(RequestBodyType requestBodyType) {
		this.requestBodyType = requestBodyType;
	}
}
