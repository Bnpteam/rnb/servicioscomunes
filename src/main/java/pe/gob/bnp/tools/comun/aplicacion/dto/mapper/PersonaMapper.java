package pe.gob.bnp.tools.comun.aplicacion.dto.mapper;

import org.springframework.stereotype.Component;

import pe.gob.bnp.tools.reniec.aplicacion.dto.PersonaDTO;
import pe.gob.bnp.tools.reniec.dominio.entidad.Persona;

@Component
public class PersonaMapper {
	public PersonaDTO mapper(Persona objeto, String coResultado, String deResultado) {
		PersonaDTO dto = new PersonaDTO(objeto, coResultado,deResultado);
		return dto;
	}
	
}
