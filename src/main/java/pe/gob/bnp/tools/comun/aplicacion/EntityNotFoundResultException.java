package pe.gob.bnp.tools.comun.aplicacion;

public class EntityNotFoundResultException extends RuntimeException{
	private static final long serialVersionUID = -7646860266726438020L;

	public EntityNotFoundResultException(String message, Throwable ex) {
		super(message, ex);
	}

	public EntityNotFoundResultException(String message) {
		super(message);
	}
}
