package pe.gob.bnp.tools.sunat.aplicacion.dto;

import pe.gob.bnp.tools.comun.aplicacion.dto.RequestBaseDto;
import pe.gob.bnp.tools.sunat.dominio.entidad.Entidad;

public class EntidadDto extends RequestBaseDto {
	private String ruc;
	private String razonSocial;
	private String departamento;
	private String provincia;
	private String distrito;
	private String direccion;
	private String ubigeo;
	private String estado;
	private String tipoEmpresa;
	private String tipoPersona;
	
	public EntidadDto() {
		super();
		this.ruc = "";
		this.razonSocial = "";
		this.departamento = "";
		this.provincia = "";
		this.distrito = "";
		this.direccion = "";
		this.ubigeo = "";
		this.estado = "";
		this.tipoPersona = "";
		this.tipoEmpresa = "";
	}	
	
	public EntidadDto(Entidad objeto) {
		super();
		this.ruc = objeto.getRuc();
		this.razonSocial = objeto.getRazonSocial();
		this.departamento = objeto.getDepartamento();
		this.provincia = objeto.getProvincia();
		this.distrito = objeto.getDistrito();
		this.direccion = objeto.getDireccion();
		this.ubigeo = objeto.getUbigeo();
		this.estado = objeto.getEstado();
		this.tipoEmpresa = objeto.getTipoEmpresa();
		this.tipoPersona = objeto.getTipoPersona();
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getDistrito() {
		return distrito;
	}

	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getUbigeo() {
		return ubigeo;
	}

	public void setUbigeo(String ubigeo) {
		this.ubigeo = ubigeo;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getTipoEmpresa() {
		return tipoEmpresa;
	}

	public void setTipoEmpresa(String tipoEmpresa) {
		this.tipoEmpresa = tipoEmpresa;
	}

	public String getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	
	
	
}
