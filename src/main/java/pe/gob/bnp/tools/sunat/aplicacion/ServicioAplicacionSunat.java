package pe.gob.bnp.tools.sunat.aplicacion;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import pe.gob.bnp.tools.comun.aplicacion.Notification;
import pe.gob.bnp.tools.comun.aplicacion.dto.mapper.EntidadMapper;
import pe.gob.bnp.tools.comun.util.VO;
import pe.gob.bnp.tools.sunat.aplicacion.dto.EntidadDto;
import pe.gob.bnp.tools.sunat.dominio.entidad.Entidad;

@Service
@CrossOrigin(origins = "*")
public class ServicioAplicacionSunat {
	@Autowired
	EntidadMapper entidadMapper;
	
	@Value("${url.api.rest.sunat.consultaruc}")
	String urlConsultaRucSunat;
	
	public EntidadDto consultarRuc(String ruc){
		EntidadDto dto = entidadMapper.mapper(this.consultarRucUsandoApiRestSunat(ruc));
		
		return dto;
	}
	
	public Entidad consultarRucUsandoApiRestSunat(String ruc) {
		Entidad objeto = new Entidad();
		Notification notification = this.validation(ruc);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage()); 
		}
		
		RestTemplate restTemplate = new RestTemplate();
		String result = (String)restTemplate.getForObject(urlConsultaRucSunat+"/DatosPrincipales?numruc=" + ruc, String.class, new Object[0]);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(result));
			Document doc = db.parse(is);
			NodeList nodes = doc.getElementsByTagName("multiRef");
			Element element = (Element)nodes.item(0);
			
			objeto.setRazonSocial(VO.getValueOfElement(element,"ddp_nombre"));	
			objeto.setRuc(VO.getValueOfElement(element,"ddp_numruc"));
			objeto.setDepartamento(VO.getValueOfElement(element,"desc_dep"));
			objeto.setProvincia(VO.getValueOfElement(element,"desc_prov"));
			objeto.setDistrito(VO.getValueOfElement(element,"desc_dist"));
			objeto.setTipoPersona(VO.getValueOfElement(element,"desc_identi"));
			objeto.setTipoEmpresa(VO.getValueOfElement(element,"desc_tpoemp"));
			objeto.setUbigeo(VO.getValueOfElement(element,"ddp_ubigeo"));
			objeto.setDireccion(VO.getValueOfElement(element,"desc_tipvia"));
			objeto.setDireccion(objeto.getDireccion()+" " +VO.getValueOfElement(element,"ddp_nomvia"));
			objeto.setDireccion(objeto.getDireccion()+" " +VO.getValueOfElement(element,"ddp_numer1"));
			objeto.setDireccion(objeto.getDireccion()+" - " +VO.getValueOfElement(element,"ddp_refer1"));
			objeto.setEstado(VO.getValueOfElement(element,"desc_estado"));
			
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
		return objeto;
	}
	
	public Notification validation(String ruc) {
		Notification notification = new Notification();
		if (VO.isEmptyString(ruc)){
			notification.addError("Debe ingresar el número de RUC"); 
		}
		if (VO.getString(ruc).length()!=11){
			notification.addError("RUC debe tener 11 dígitos"); 
		}
		return notification;
	}
	
}
