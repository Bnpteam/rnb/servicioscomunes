package pe.gob.bnp.tools.sunat.api.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.bnp.tools.comun.api.controlador.ResponseHandler;
import pe.gob.bnp.tools.comun.aplicacion.EntityNotFoundResultException;
import pe.gob.bnp.tools.comun.aplicacion.enumeracion.RequestBodyType;
import pe.gob.bnp.tools.sunat.aplicacion.ServicioAplicacionSunat;
import pe.gob.bnp.tools.sunat.aplicacion.dto.EntidadDto;

@RestController
@RequestMapping("api/serviciosSunat/")
@Api(value = "/api/serviciosSunat/",description="Servicio REST de consulta a SUNAT - Desarrollado por OTIE/BNP")
@CrossOrigin(origins = "*")
public class SunatServicesController {
	@Autowired
	ServicioAplicacionSunat servicioAplicacionSunat;
	
	@Autowired
	ResponseHandler responseHandler;
	
	@RequestMapping(method = RequestMethod.GET, path = "consultarRUC/{ruc}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Consulta de RUC", response= EntidadDto.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 400, message = "Solicitud contiene errores."),
        @ApiResponse(code = 404, message = "No se pudo obtener datos de la entidad."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la petición"),
	})		
	public ResponseEntity<Object> consultarRUC(
			@ApiParam(value = "número de RUC consultado", required = true)
			@PathVariable("ruc") String dni){ 
		try {
			EntidadDto entidadDto = servicioAplicacionSunat.consultarRuc(dni);
			if (entidadDto== null) {
				return this.responseHandler.getNotFoundObjectResponse("No se ha encontrado información para el RUC ingresado.");
			}
			entidadDto.setRequestBodyType(RequestBodyType.VALID);
			return this.responseHandler.getOkObjectResponse(entidadDto);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("Entidad no encontrada", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}
	}	

}
