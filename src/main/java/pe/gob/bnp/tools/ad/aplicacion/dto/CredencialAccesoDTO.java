package pe.gob.bnp.tools.ad.aplicacion.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import pe.gob.bnp.tools.ad.aplicacion.dto.deserializer.CredencialAccesoDTODeserializer;
import pe.gob.bnp.tools.comun.aplicacion.dto.RequestBaseDto;
import pe.gob.bnp.tools.comun.aplicacion.enumeracion.RequestBodyType;

@JsonDeserialize(using = CredencialAccesoDTODeserializer.class)
public class CredencialAccesoDTO extends RequestBaseDto {
	private String usuario;
	private String password;
	
	public CredencialAccesoDTO() {
	}	
	
	public CredencialAccesoDTO(String usuario, String password) {
		super();
		this.usuario = usuario;
		this.password = password;
	}	

	public CredencialAccesoDTO(String usuario, String password, RequestBodyType requestBodyType) {
		super();
		this.usuario = usuario;
		this.password = password;
		this.requestBodyType = requestBodyType;
	}	
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	
	
	
}
