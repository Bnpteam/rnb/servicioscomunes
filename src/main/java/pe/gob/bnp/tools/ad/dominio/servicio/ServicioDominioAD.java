package pe.gob.bnp.tools.ad.dominio.servicio;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import org.springframework.stereotype.Service;

import pe.gob.bnp.tools.ad.aplicacion.dto.AccesoLdapResponse;
import pe.gob.bnp.tools.ad.aplicacion.dto.CredencialAccesoDTO;

@Service
public class ServicioDominioAD {
	
	@SuppressWarnings({ "unchecked", "unused" })
	public AccesoLdapResponse validarUsuarioenAD(CredencialAccesoDTO credencialAccesoDTO){//aqui viene la integración con Active Directory
		
		AccesoLdapResponse response = null;

		@SuppressWarnings("rawtypes")
		Hashtable credenciales = new Hashtable(11);
		DirContext contexto = null;
		try {
			credenciales.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
			credenciales.put(Context.SECURITY_AUTHENTICATION,"simple");
			credenciales.put(Context.SECURITY_PRINCIPAL, credencialAccesoDTO.getUsuario()+"@bnp.gob.pe");
			credenciales.put(Context.SECURITY_CREDENTIALS, credencialAccesoDTO.getPassword());
			credenciales.put(Context.PROVIDER_URL, "ldap://172.16.88.203:389");
			
			contexto = new InitialDirContext(credenciales);
			response = new AccesoLdapResponse();
			if (contexto!=null) {
				response.setCodigoRespuesta("0000");
				response.setDescripcionRespuesta("El usuario y contraseña son correctos.");
			}else {
				response.setCodigoRespuesta("0001");
				response.setDescripcionRespuesta("El nombre de usuario o contraseña no es correcto.");
			}
		} catch (NamingException e) {
			response = new AccesoLdapResponse();
			this.obtenerMotivoRechazoEnAD(response,new String(e.toString()));
			System.out.println("Error en e.getMessage: "+e.getMessage());
			/*e.printStackTrace();*/
		} finally {
			if (contexto!=null) {
				try {
					contexto.close();
				} catch (NamingException e) {
					e.printStackTrace();
				}
			}
		}
		return response;
	}	
	
	public void obtenerMotivoRechazoEnAD(AccesoLdapResponse response, String cadena) {
		int pos = cadena.indexOf("data ");
		String descripcionRechazo = "";
		if (pos>0) {
			String campo1= cadena.substring(pos+5,pos+5+3).concat("");
			if (campo1.equals("52e")){
				//System.out.println("Valor 52e SI es igual a "+campo1);
				descripcionRechazo = "El nombre de usuario o contraseña no es correcto.";
			}	
			if (campo1.equals("775")){
				//System.out.println("Valor 52e SI es igual a "+campo1);
				descripcionRechazo = "La cuenta a que se hace referencia está bloqueada y no se puede utilizar.";
			}			
			if (campo1.equals("525​")){
				descripcionRechazo = "Usuario no existe.";
			}

			if (campo1.equals("530​") || campo1.equals("531​")){
				descripcionRechazo = "Intento de logueo no permitido.";
			}			
			if (campo1.equals("532​")){
				descripcionRechazo = "Contraseña expirada.";
			}	
			if (campo1.equals("533​")){
				descripcionRechazo = "Cuenta deshabilitada.";
			}			
			if (campo1.equals("701​")){
				descripcionRechazo = "Cuenta expirada.";
			}
			if (campo1.equals("773​")){
				descripcionRechazo = "Debe resetear su contraseña.";
			}				
		
			response.setCodigoRespuesta(campo1);
			response.setDescripcionRespuesta(descripcionRechazo);
		}else {
			response.setCodigoRespuesta("999");
			response.setDescripcionRespuesta("No se pudo obtener él código de rechazo");
		}
	}
}
