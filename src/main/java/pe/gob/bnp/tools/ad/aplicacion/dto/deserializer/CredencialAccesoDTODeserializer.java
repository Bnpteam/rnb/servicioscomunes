package pe.gob.bnp.tools.ad.aplicacion.dto.deserializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import pe.gob.bnp.tools.ad.aplicacion.dto.CredencialAccesoDTO;
import pe.gob.bnp.tools.comun.aplicacion.enumeracion.RequestBodyType;

public class CredencialAccesoDTODeserializer extends JsonDeserializer<CredencialAccesoDTO> {

	@Override
	public CredencialAccesoDTO deserialize(JsonParser jsonParser, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		CredencialAccesoDTO dto = null;
		try {
			ObjectCodec objectCodec = jsonParser.getCodec();
			JsonNode jsonNode = objectCodec.readTree(jsonParser);	
			String usuario = jsonNode.get("usuario").asText();
			String password = jsonNode.get("password").asText();
			dto = new CredencialAccesoDTO(usuario,password, RequestBodyType.VALID);
		} catch (Exception e) {
			dto = new CredencialAccesoDTO("","", RequestBodyType.INVALID);
		}
		return dto;
	}

}
