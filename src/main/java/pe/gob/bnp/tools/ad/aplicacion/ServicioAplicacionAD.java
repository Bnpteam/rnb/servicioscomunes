package pe.gob.bnp.tools.ad.aplicacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.bnp.tools.ad.aplicacion.dto.AccesoLdapResponse;
import pe.gob.bnp.tools.ad.aplicacion.dto.CredencialAccesoDTO;
import pe.gob.bnp.tools.ad.dominio.servicio.ServicioDominioAD;
import pe.gob.bnp.tools.comun.aplicacion.Notification;
import pe.gob.bnp.tools.comun.aplicacion.enumeracion.RequestBodyType;

@Service
public class ServicioAplicacionAD {
	@Autowired
	ServicioDominioAD servicioDominioAD;
	
	//@Transactional
	public AccesoLdapResponse validarUsuarioenAD(CredencialAccesoDTO credencialAccesoDTO) throws Exception {
		Notification notification = this.validation(credencialAccesoDTO);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}
		AccesoLdapResponse response= this.servicioDominioAD.validarUsuarioenAD(credencialAccesoDTO);
		return response;	
	}
	
	private Notification validation(CredencialAccesoDTO credencialAccesoDTO) {
		Notification notification = new Notification();
		if (credencialAccesoDTO == null) {
			notification.addError("Request Body inválido.");
			return notification;
		}
		if (credencialAccesoDTO.getRequestBodyType() != RequestBodyType.VALID)   {
			notification.addError("Datos JSON inválidos.");
			return notification;
		}
		if (credencialAccesoDTO.getUsuario() == null || credencialAccesoDTO.getUsuario().equals("")) {
			notification.addError("Escriba un nombre de usuario y una contraseña.");
			return notification;
		}
		if (credencialAccesoDTO.getPassword() == null || credencialAccesoDTO.getPassword().equals("")) {
			notification.addError("Escriba un nombre de usuario y una contraseña.");
			return notification;
		}
		return notification;
	}		
}
