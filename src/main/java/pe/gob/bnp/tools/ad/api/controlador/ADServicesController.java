package pe.gob.bnp.tools.ad.api.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.bnp.tools.ad.aplicacion.ServicioAplicacionAD;
import pe.gob.bnp.tools.ad.aplicacion.dto.AccesoLdapResponse;
import pe.gob.bnp.tools.ad.aplicacion.dto.CredencialAccesoDTO;
import pe.gob.bnp.tools.comun.api.controlador.ResponseHandler;
import pe.gob.bnp.tools.comun.aplicacion.EntityNotFoundResultException;

@RestController
@RequestMapping("api/serviciosAD/")
@CrossOrigin(origins = "*")
@Api(value = "/api/serviciosAD/",description="Servicio REST de autenticación con Active Directory - Implementado por OTIE/BNP")
public class ADServicesController {
	@Autowired
	ServicioAplicacionAD servicioAplicacionAD;
	
	@Autowired
	ResponseHandler responseHandler;
	
	//@CrossOrigin(origins = "http://172.16.88.209:4200")
	@RequestMapping(method = RequestMethod.POST, path = "/validarUsuario", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Validar si el usuario esta registrado y habilitado en Active Directory", response= AccesoLdapResponse.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Operación exitosa"),
        @ApiResponse(code = 400, message = "Solicitud contiene errores"),
        @ApiResponse(code = 404, message = "Error al momento de realizar consulta a Active Directory"),
        @ApiResponse(code = 500, message = "Error al momento de ejecutar la petición"),
	})	
	public ResponseEntity<Object> validarAccesoActiveDirectory(
			@ApiParam(value = "bean que contiene el usuario y password ingresado por el usuario", required = true)
			@RequestBody  CredencialAccesoDTO credenciales){ 
		try {
			AccesoLdapResponse response = servicioAplicacionAD.validarUsuarioenAD(credenciales);
			if (response == null) {
				return this.responseHandler.getNotFoundObjectResponse("Error al momento de realizar consulta a Active Directory.");
			}
			return this.responseHandler.getOkObjectResponse(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("Error al momento de realizar consulta a Active Directory.", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}
	}
	
	
	//@CrossOrigin(origins = "http://172.16.88.209:4200")
	/*@RequestMapping(method = RequestMethod.GET, path = "/test", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> validarAccesoActiveDirectory(){ 
		try {
			return this.responseHandler.getOkCommandResponse("hello world");
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("Error al momento de realizar consulta a Active Directory.", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}
	}	*/
}
