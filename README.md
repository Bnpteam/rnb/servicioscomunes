## SERVICIOS 2 COMUNES
### Descripción
- Backend de servicios comunes consultas PIDE.
- Desde la página principal del RNB http://rnb.bnp.gob.pe/#/home-page  se puede visualizar consultas de bibliotecas a través de 2 tipos de usuarios. 
    Dentro del flujo del rnb es necesario obtener datos de usuarios, responsable bibliotecas, etc a través de el dni o ruc ingresado según corresponda.

### Configuración

- Para configurar en el ambientes de TEST:

 * Dentro del Proyecto servicioscomunes cambiar parametro spring.profiles.active a "development" en el archivo application.properties.
 * Desactivar ssl archivo principal CommonServicesApplication y parametros server.ssl.
 * IDE - STS 4.0.
 * TOMCAT 9.0 EMBEBIDO.
 * JDK 1.8.0.261
 * RUTA: http://rnb.bnp.gob.pe:8081/servicioscomunes/swagger-ui.html#/
  
- Para configurar en el ambiente de PROD:

 * Dentro del Proyecto servicioscomunes cambiar parametro spring.profiles.active a "production" en el archivo application.properties.
 * Activar ssl archivo principal CommonServicesApplication y parametros server.ssl. (cambio de puerto)
 * IDE - STS 4.0.
 * TOMCAT 9.0 EMBEBIDO.
 * JDK 1.8.0.261
 * RUTA: https://apicomunes.bnp.gob.pe:9884/servicioscomunes/swagger-ui.html#/

### Despliegue Microservicio

- Utilizar el archivo servicioscomunes-be.service como base para desplegar el microservicio
- Pasos de despliegue:
    1. Generar jar del aplicativo servicioscomunes (mave clean build install).
    2. Copiar el jar dentro del servidor test o prod que desee en la ruta indicada por el archivo servicioscomunes-be.service. (dar permisos escritura y lectura al jar)
    3. Generar en caso no exista un servicio en linux con el nombre del archivo servicioscomunes-be.service.
        * Para crear servicio en linux, acceder a la ruta /etc/systemd validar los servicios existentes y copiar con el comando "cp" servicio existente actualizar       las rutas para que apunten al jar creado. 
    4. Una vez creado o actualizado el archivo .service reiniciarlo de preferencia utilizar los comando:
        systemctl status servicioscomunes-be.service (ver el estado actual)
        systemctl stop   servicioscomunes-be.service (detener servicio)
        systemctl start  servicioscomunes-be.service (inciar servicio)
    
    5. Finalmente validar los servicios desde la ruta generado por el swagger.

 
#### version: v1.0.2